import com.google.common.base.CaseFormat
import groovy.transform.Field

def tableName(def entityName, def prefix = null) {
    if (!prefix) {
//        def appName = getAppName()
//        prefix = appName ? appName.substring(0, appName.length() > 2 ? 3 : appName.length()) : "T";
        return "${CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, entityName)}";
    } else {
        return "${prefix.toUpperCase()}_${CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, entityName)}";
    }
}

// Tuple3(h2, mysql, pg)
// https://docs.oracle.com/cd/E19501-01/819-3659/gcmaz/
// http://www.h2database.com/html/datatypes.html
// https://www.postgresql.org/docs/current/datatype.html
// https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-reference-type-conversions.html
@Field
def dbDialect = [
        "java.lang.String"       : new Tuple3<>("VARCHAR(10)", "VARCHAR(10)", "VARCHAR(10)"),
        "char"                   : new Tuple3<>("CHAR(10)", "CHAR(10)", "CHAR(10)"),
        "java.lang.Boolean"      : new Tuple3<>("BIT", "BIT", "BOOLEAN"),
        "boolean"                : new Tuple3<>("BIT", "BIT", "BOOLEAN"),
        "byte[]"                 : new Tuple3<>("BLOB", "BINARY", "bytea"),
        "java.lang.Short"        : new Tuple3<>("SMALLINT", "SMALLINT", "SMALLINT"),
        "short"                  : new Tuple3<>("SMALLINT", "SMALLINT", "SMALLINT"),
        "java.lang.Integer"      : new Tuple3<>("INTEGER", "INTEGER", "INTEGER"),
        "int"                    : new Tuple3<>("INTEGER", "INTEGER", "INTEGER"),
        "java.lang.Long"         : new Tuple3<>("BIGINT", "BIGINT", "BIGINT"),
        "long"                   : new Tuple3<>("BIGINT", "BIGINT", "BIGINT"),
        "java.lang.Float"        : new Tuple3<>("FLOAT", "FLOAT", "REAL"),
        "float"                  : new Tuple3<>("FLOAT", "FLOAT", "REAL"),
        "java.lang.Double"       : new Tuple3<>("NUMERIC(10,3)", "NUMERIC(10,3)", "NUMERIC(10,3)"),
        "double"                 : new Tuple3<>("NUMERIC(10,3)", "NUMERIC(10,3)", "NUMERIC(10,3)"),
        "java.lang.BigDecimal"   : new Tuple3<>("NUMERIC(10,3)", "NUMERIC(10,3)", "NUMERIC(10,3)"),
        "java.lang.BigInteger   ": new Tuple3<>("BIGINT", "BIGINT", "DECIMAL(10,3)"),
        "java.sql.Time"          : new Tuple3<>("TIME", "TIME", "time"),
        "java.sql.Date"          : new Tuple3<>("DATE", "DATE", "DATE"),
        "java.sql.Timestamp"     : new Tuple3<>("TIMESTAMP", "TIMESTAMP", "TIMESTAMP"),
        "byte[]"                 : new Tuple3<>("BINARY", "BLOB(1024)", "bytea"),
        // sequence for database
        "SEQUENCE"               : new Tuple3<>("AUTO_INCREMENT", "AUTO_INCREMENT", "SERIAL"),
] as Map


def getDBTypeMapping(def dir) {
    def dbMap = ["mysql-connector-java": "mysql", "postgresql": "pg"]
    def pom = new File(dir, "pom.xml")

    def root = new XmlSlurper().parseText(pom.text)

    def node = root.dependencies.dependency.find {
        dbMap.keySet().contains(it.artifactId)
    }
    def db = dbMap.get(node.artifactId.text())
    def rs = ["h2": dbDialect.entrySet().collectEntries { [(it.key): it.value.v1] }] as LinkedHashMap
    if ("mysql".equalsIgnoreCase(db)) {
        rs.put("mysql", dbDialect.entrySet().collectEntries { [(it.key): it.value.v2] })
    } else if ("pg".equalsIgnoreCase(db)) {
        rs.put("pg", dbDialect.entrySet().collectEntries { [(it.key): it.value.v3] })
    }
    return rs
}





