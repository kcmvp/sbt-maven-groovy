println('''
** Description
This mojo supplies two main utils:
1: Generate schema based on POJO.
2: Generate mybatis helper xml mapper

** Usage
1: sbt:mybatis -Daction=schema -Dentity=xxx 
   Generate scheam for an entity; for example sbt:mybatis -Daction=schema -Dentity=com.abc.Order
2: sbt:mybatis -Daction=mapper -Dentity=xxx.class
   Generate helper used xml mapper for an entity; for example sbt:mybatis -Daction=mapper -Dentity=com.abc.Order   
''')
