#! /usr/bin/env groovy
@Grapes([
        @Grab(group = 'com.google.guava', module = 'guava', version = '18.0'),
        @Grab(group = 'jaxen', module = 'jaxen', version = '1.1.4')
])

import com.google.common.base.CaseFormat
import groovy.transform.Field

/*
println session.getUserProperties()['action']
println session.getUserProperties()['entity']
 */

def currentPath = new File(getClass().protectionDomain.codeSource.location.path).parent
GroovyShell groovyShell = new GroovyShell()
def app = groovyShell.parse(new File(currentPath, "../common/app.groovy"))
def util = groovyShell.parse(new File(currentPath, "util.groovy"))

def linebreak = System.getProperty("line.separator");
def resourceDir = project.build.resources[0].directory
def schemaFolder = new File(resourceDir, "schema");
if (!schemaFolder.exists()) schemaFolder.mkdirs()


def genSchema = { clz, dbm ->
    def tableName = util.tableName(clz.simpleName)
    def columnOrderMap = [:]
    def sdf = new java.text.SimpleDateFormat("yyyyMMdd_HHmmss");
    def namePrefix = "V${sdf.format(Calendar.instance.getTime())}"
    dbm.entrySet().each { Map.Entry entry ->
        def oldFile = null
        def columnTypeMap = [:]
        def typeMap = entry.value
        schemaFolder.eachFileRecurse { f ->
            if (f.name.indexOf("${tableName}-${entry.key}.sql") > -1) {
                oldFile = f
                f.eachWithIndex { line, idx ->
                    if (line && idx != 0) {
                        if (line.indexOf("ENGINE") < 0 && line.split().length > 1) {
                            line = line.subSequence(0, line.length() - 1);
                            def entries = line.split();
                            columnTypeMap.put(entries[0], entries[1])
                            //
                            columnOrderMap.putIfAbsent(entries[0], idx);
                        }
                    }
                }
            }
        }
        def hasId = false
        def sb = new StringBuffer("CREATE TABLE ${tableName}(");
        clz.metaClass.properties.sort {
            columnOrderMap.get(CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, it.name)) ?: it.name.length()
        }.eachWithIndex { prop, idx ->
            if (!prop.name.equals("class")) {
                def claz = (Class) prop.type
                def type = typeMap.get(prop.type.name);
                def column = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, prop.name);
                if (!type) {
                    if (claz.isEnum()) {
                        // enum
                        def fields = claz.getDeclaredFields()
                        def b = fields.stream().filter { p ->
                            p.type.name.indexOf(claz.name) < 0
                        }.findFirst().orElse(null)
                        type = typeMap.get(b.type.name)
                        column = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, prop.name);
                        // collection
                    } else if (java.util.Collection.class.isAssignableFrom(claz)) {
                        println "**Info**: ${prop.name} (${prop.type}) is assigned from  java.util.Collection"
                        type = "VARCHAR(20)"
                        column = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, prop.name);
                        // customize type
                    } else {
                        println "**Info**: ${prop.name} (${prop.type}) is not a built-in Type"
                        column = column + "_ID"
                        type = "BIGINT"
                    }
                }
                type = columnTypeMap.get(column) ?: type;
                sb.append("${linebreak}").append("\t")
                if (prop.name.equals("id")) {
                    hasId = true
                    if (["h2", "mysql"].contains(entry.key)) {
                        sb.append("ID BIGINT NOT NULL AUTO_INCREMENT,")
                    } else {
                        sb.append("ID SERIAL,")
                    }
                } else {
                    sb.append("${column} ${type},")
                }
            }
        }
        sb.append("${linebreak}").append("\t")
        if (hasId) sb.append("PRIMARY KEY (ID)")
        sb.append("${linebreak})")
        if (entry.key.equalsIgnoreCase("mysql")) sb.append(" ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin")
        sb.append(";")
        if (oldFile) oldFile.delete()
        def sql = new File("${schemaFolder}/${namePrefix}-${tableName}-${entry.key}.sql");
        sql.newWriter().withWriter { w ->
            w << sb.toString();
        }
    }
}

def consolidateSchema = { dbm ->
    def checkList = ["VARCHAR", "CHAR", "NUMERIC"]
    def metaMap = [:] as Map<String, String>
    def bufferedMap = [:] as Map

    dbm.keySet().forEach { db ->
        def sql = new File("${resourceDir}/schema-${db}.sql");
        if (sql.exists()) sql.delete()
    }
    dbm.keySet().forEach { db ->
        def sb = new StringBuffer()
        bufferedMap.put("${resourceDir}/schema-${db}.sql", sb)
        schemaFolder.listFiles().findAll { f -> (f.name.indexOf(db) > -1 && f.name.indexOf(".sql") > -1) }.collect().sort { f -> f.name }.each { f ->
            def tableName = f.name.split("-")[1].toUpperCase()
            f.eachWithIndex { line, idx ->
                if (!line.startsWith("CREATE TABLE") && !line.startsWith(")")) {
                    def entries = line.trim().split()
                    def o = metaMap.putIfAbsent("${tableName}.${entries[0].toUpperCase()}", entries[1])
                    if (o && checkList.find { c -> o.startsWith(c) }) {
                        if (!o.equalsIgnoreCase(entries[1])) {
                            throw new RuntimeException("precision does not match: ${tableName}.${entries[0].dropRight(1)} : ${o.dropRight(1)}/${entries[1].dropRight(1)}")
                        }
                    }
                }
                sb.append(line).append(linebreak)
            }
        }
    }
    bufferedMap.entrySet().each {
        def sql = new File(it.key)
        sql.newWriter().withWriter { w ->
            w << it.value.toString();
        }
    }
}


println "Generating schema for: ${session.getUserProperties()['entity']}"
def dbm = util.getDBTypeMapping(project.basedir) as Map

def clzList = app.loadEntityClaz(project.build.outputDirectory, session.getUserProperties()['entity']) as List<Class>

clzList.eachWithIndex { Class entryClz, int i ->
    if (!Enum.class.isAssignableFrom(entryClz) && !entryClz.name.toLowerCase().startsWith("base")) {
        genSchema(entryClz, dbm)
    }
}
consolidateSchema(dbm)








