#! /usr/bin/env groovy
@Grapes([
        @Grab(group = 'com.google.guava', module = 'guava', version = '18.0'),
        @Grab(group = 'jaxen', module = 'jaxen', version = '1.1.4')
])

import com.google.common.base.CaseFormat
import groovy.xml.MarkupBuilder
import groovy.xml.XmlUtil

import java.text.SimpleDateFormat
import java.util.regex.Matcher
import java.util.regex.Pattern

def currentPath = new File(getClass().protectionDomain.codeSource.location.path).parent
GroovyShell groovyShell = new GroovyShell()
def app = groovyShell.parse(new File(currentPath, "../common/app.groovy"))
def util = groovyShell.parse(new File(currentPath, "util.groovy"))
def linebreak = System.getProperty("line.separator");



def indentSpace = " " * 2
def genInsert = {clz ->

    def tableName = util.tableName(clz.simpleName)
    def insertBuffer = new StringBuffer(linebreak);
    def valueBuffer = new StringBuffer("${indentSpace*2}values (")
    insertBuffer.append("${indentSpace*2}INSERT INTO ${tableName}(")
            //.append(linebreak);
    def size = clz.metaClass.properties.size();
    clz.metaClass.properties.eachWithIndex { prop, idx ->
        def claz = (Class) prop.type
        if (!prop.name.equals("id") && !prop.name.equals("class") && !java.util.Collection.class.isAssignableFrom(claz)) {
            def column = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, prop.name);
            insertBuffer.append(column);
            if (idx + 1 < size) {
                insertBuffer.append(",");
            }
            if (prop.name.equals("createdAt") || prop.name.equals("updatedAt")) {
                valueBuffer.append("CURRENT_TIMESTAMP()")
            } else {
                valueBuffer.append("#{").append("${prop.name}").append("}")
            }
            if (idx + 1 < size) {
                valueBuffer.append(",")
            }
        }

    }
    insertBuffer.append(")").append(linebreak).append(valueBuffer).append(")").append(linebreak)
    return insertBuffer.toString();

}

def genSelectById = {clz ->

    def tableName = util.tableName(clz.simpleName)
    def selectBuffer = new StringBuffer(linebreak).append("${indentSpace*4}SELECT ");
    def size = clz.metaClass.properties.size();
    clz.metaClass.properties.eachWithIndex { prop, idx ->

        def claz = (Class) prop.type
        if (!prop.name.equals("class") && !java.util.Collection.class.isAssignableFrom(claz)) {
            def column = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, prop.name);
            selectBuffer.append(column);
            if (idx + 1 < size) {
                selectBuffer.append(",");
            }
        }

    }
    selectBuffer.append(linebreak).append("${indentSpace*4}FROM ${tableName}")
            .append(linebreak).append("${indentSpace*4}WHERE ID = #{id}").append(linebreak)
    return selectBuffer.toString();
}
def genUpdate = { clz ->

    def tableName = util.tableName(clz.simpleName)

    def ignoredProperties = ["createdAt", "createdBy", "id", "class", "updatedAt"]
    def updateBuffer = new StringBuffer(linebreak);
    updateBuffer.append("${indentSpace*2}UPDATE ${tableName}").append(linebreak);
    updateBuffer.append("${indentSpace*2}<set>").append(linebreak)
    def size = clz.metaClass.properties.size();
    clz.metaClass.properties.eachWithIndex { prop, idx ->
        def claz = (Class) prop.type
        if (!ignoredProperties.contains(prop.name) && !java.util.Collection.class.isAssignableFrom(claz)) {
            def column = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, prop.name);

            updateBuffer.append("${indentSpace*4}<if test=\"${prop.name} != null\">").append(linebreak)
            updateBuffer.append("\t").append("${indentSpace*4}${column} = #{").append(prop.name).append("}")
            if (idx + 1 < size) {
                updateBuffer.append(",");
            }
            updateBuffer.append(linebreak).append("${indentSpace*4}</if>").append(linebreak);
        }

    }
    updateBuffer.append("${indentSpace*4}UPDATED_AT = CURRENT_TIMESTAMP()").append(linebreak)
    updateBuffer.append("${indentSpace*2}</set>");
    updateBuffer.append(linebreak).append("${indentSpace*2}WHERE ID = #{id}").append(linebreak)
    return updateBuffer.toString();
}


def resourceDir = project.build.resources[0].directory
def entity = session.getUserProperties()['entity']
if (!entity)
    throw new RuntimeException("-Dmybatis.entity can not be empty")

def clzList = app.loadEntityClaz(project.build.outputDirectory, entity) as List<Class>
if (clzList.isEmpty())
    throw new RuntimeException("Can not find the entity ${entity}")

def mapperList = app.getAnnotationProperties(project.build.outputDirectory, "MapperScan", "value")
if (!mapperList) {
    throw new RuntimeException("Can not find class annotated with @MapperScan!")
}

def mapperPackage = mapperList[0]

def mapperPath = new File(project.build.sourceDirectory, mapperPackage.replaceAll(Pattern.quote("."), Matcher.quoteReplacement(File.separator)));
if (!mapperPath.exists()) mapperPath.mkdirs()
def mapper = new File(mapperPath, "${entity}Mapper.java");
if (mapper.exists() && !mapper.isDirectory()) {
    println "**Warning**: Mapper ${mapper.absolutePath} exists";
} else {
    def fw = new FileWriter(mapper);
    def bw = new BufferedWriter(fw);
    def now = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().time)
    bw.write("package ${mapperPackage};");
    bw.newLine();
    bw.write("import ${clzList[0].getCanonicalName()};")
    bw.write(linebreak)
    bw.write(" ")
    bw.newLine()
    bw.write("/**")
    bw.newLine()
    bw.write("* @author ${System.getProperty("user.name")}")
    bw.newLine()
    bw.write("* @version ${project.version}")
    bw.newLine()
    bw.write("* @date ${now}")
    bw.newLine()
    bw.write("*/")
    bw.newLine()
    bw.write(" ")
    bw.newLine()
    bw.write("public interface ${entity}Mapper {")
    bw.newLine()
    bw.write("\t");
    bw.write("Long create${entity}(${entity} ${entity.toLowerCase()});");
    bw.newLine()
    bw.write("\t");
    bw.write("Integer update${entity}(${entity} ${entity.toLowerCase()});");
    bw.newLine()
    bw.write("\t");
    bw.write("${entity} find${entity}ById(Long id);");
    bw.newLine()
    bw.write("}")
    bw.newLine()
    bw.close()
    println "**Info**: ${mapper.path}"
}


println "[mapper] entity: ${session.getUserProperties()['entity']}"
def parser = new XmlParser();
parser.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

def mapperXmlPath = new File(resourceDir, mapperPackage.replaceAll(Pattern.quote("."), Matcher.quoteReplacement(File.separator)));
if (!mapperXmlPath.exists()) mapperXmlPath.mkdirs()
def mapperXml = new File(mapperXmlPath, "${entity}Mapper.xml");
if (mapperXml.exists() && !mapperXml.isDirectory()) {
    println "**Warning**: Mapper ${mapperXml.absolutePath} exists";
    def response = parser.parseText(mapperXml.getText());

    println "................................................................"
    def node = parser.createNode(
            response,
            "insert",
            [id: "create${entity}", parameterType: "${entity}", keyProperty: "id", useGeneratedKeys: "true"]
    )
    node.setValue(genInsert(clzList.get(0)));
    new NodePrinter(preserveWhitespace: true).print(node);


    println "................................................................"
    node = parser.createNode(
            response,
            "update",
            [id: "update${entity}", parameterType: "${entity}"]
    )
    node.setValue(genUpdate(clzList.get(0)))
    def xmlOutput = new StringWriter()
    def xmlNodePrinter = new XmlNodePrinter(new PrintWriter(xmlOutput))
    xmlNodePrinter.print(node)
    println xmlOutput.toString().replaceAll("&lt;", "<").replaceAll("&gt;", ">");

    println "................................................................"
    node = parser.createNode(
            response,
            "select",
            [id: "get${entity}ById", resultType: "${entity}"]
    )
    node.setValue(genSelectById(clzList.get(0)));
    new NodePrinter(preserveWhitespace: true).print(node);
} else {
    def writer = new StringWriter()
    def indentPrinter = new IndentPrinter(new PrintWriter(writer), indentSpace, true)
    def xml = new MarkupBuilder(indentPrinter)
    xml.setEscapeAttributes(true);
    def xmlHelper = new groovy.xml.MarkupBuilderHelper(xml)
    xmlHelper.xmlDeclaration([version: '1.0', encoding: 'UTF-8', standalone: 'no'])
    xmlHelper.yieldUnescaped '<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >'

    xml.mapper(namespace: "${mapperPackage}.${entity}Mapper") {
        insert(id: "create${entity}", parameterType: "${clzList[0].getCanonicalName()}", keyProperty: "id", useGeneratedKeys: "true", genInsert(clzList.get(0)))
        mkp.yield "\r\n"
        update(id: "update${entity}", parameterType: "${clzList[0].getCanonicalName()}", genUpdate(clzList.get(0)))
        mkp.yield "\r\n"
        select(id: "find${entity}ById", resultType: "${clzList[0].getCanonicalName()}", flushCache: "true", genSelectById(clzList.get(0)))
        mkp.yield "\r\n"
    }

    mapperXml << XmlUtil.serialize(writer.toString().replaceAll("&lt;", "<").replaceAll("&gt;", ">"))
    println "**Info**: ${mapperXml.getAbsolutePath()}"


}


