#! /usr/bin/env groovy
@Grapes([
        @Grab(group = 'com.google.guava', module = 'guava', version = '18.0'),
        @Grab(group = 'jaxen', module = 'jaxen', version = '1.1.4'),
        @Grab(group = 'org.yaml', module = 'snakeyaml', version = '1.25')
])

import groovy.io.FileType
import org.yaml.snakeyaml.Yaml

import java.util.regex.Matcher

def loadEntityClaz(String buildDir, String clazName) {
    def clazList = [] as List
    new File("${buildDir}").eachDirRecurse { dir ->
        if (dir.name.equals("entity")) {
            def packageName = dir.absolutePath.substring(buildDir.length() + 1).replaceAll(Matcher.quoteReplacement(File.separator), ".")
            dir.eachFile { f ->
                if (!clazName || f.name.startsWith(clazName)) {
                    def clzString = "${packageName.toString()}.${f.name.split("\\.")[0]}"
                    try {
                        clazList.add(this.class.classLoader.loadClass(clzString, true, false))
                    } catch (Exception) {
                        throw new RuntimeException("************* ERROR: Can't not find this class ${clzString} *************")
                    }
                }
            }
        }
    }
    return clazList
}

def getAnnotationProperties(String buildDir, String name, String propName) {
    def clzOutPut = new File(buildDir);
    def valueList = []
    clzOutPut.eachFileRecurse(FileType.FILES) {
        if (it.name.endsWith(".class")) {
            def clzName = it.absolutePath.substring(buildDir.length() + 1)
                    .reverse().drop(6).reverse().replaceAll(Matcher.quoteReplacement(File.separator), ".")
            def clzT = this.class.classLoader.loadClass(clzName, true, false)
            clzT.getAnnotations().each { a ->
                if (a.toString().indexOf(name) > -1) {
                    def v = a."${propName}"()
                    if (v instanceof java.util.Collection) {
                        valueList.addAll(v)
                    } else if (Object[].isAssignableFrom(v.getClass())){
                        valueList.addAll(Arrays.asList(v))
                    } else {
                        valueList <<v
                    }
                }
            }
        }
    }
    return valueList;
}

def findClassByAnnotationName(String buildDir, String... names) {
    def clzOutPut = new File(buildDir);
    def tupleList = [] as List<Tuple2>
    clzOutPut.eachFileRecurse(FileType.FILES) {
        if (it.name.endsWith(".class")) {
            def clzName = it.absolutePath.substring(buildDir.length() + 1)
                    .reverse().drop(6).reverse().replaceAll(Matcher.quoteReplacement(File.separator), ".")
            def clzT = this.class.classLoader.loadClass(clzName, true, false)
            clzT.getAnnotations().each { a ->
                names.toList().forEach { name ->
                    if (a.toString().indexOf(name) > -1) {
                        tupleList << [first: clzT, second: a]
                    }
                }
            }
        }
    }
    return tupleList;
}

def annotationValue(String buildDir, String annotation) {
    def clzOutPut = new File(buildDir);
    def value = ""
    clzOutPut.eachFileRecurse(FileType.FILES) {
        if (it.name.endsWith(".class")) {
            def clzName = it.absolutePath.substring(buildDir.length() + 1)
                    .reverse().drop(6).reverse().replaceAll(Matcher.quoteReplacement(File.separator), ".")
            println("clz name :${clzName}")
            def clzT = this.class.classLoader.loadClass(clzName, true, false)

            clzT.getAnnotations().each {
                println("annotation -> ${it.toString()}")
                if (it.toString().indexOf(annotation) > -1) {
                    println("value -> ${it.value()}")
                    value = it.basePackages()[0]
                }
            }
        }
    }
    return value;
}

def getAppName() {
    def url = this.class.classLoader.getResource('application.yml')
    def appName = ""
    if (url) {
        def cfg = new File(url.toURI());
        Yaml parser = new Yaml()
        def settings = parser.load(cfg.getText("UTF-8"))
        appName = settings.get("spring").get("application").get("name")
    } else {
        url = getClass().getResource('application.properties')
        if (url) {
            cfg = new File(getClass().getResource('application.properties').toURI());
            def prop = new Properties()
            cfg.withInputStream {
                prop.load(it)
            }
            appName = prop.get("spring.application.name")
        }
    }
    if (!appName)
        throw new RuntimeException("Can't find application.yml / application.properties or there is no 'spring.application.name' defined !")
    return appName
}



